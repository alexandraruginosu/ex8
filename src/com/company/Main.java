package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> myHotelEmployees = new ArrayList();

        myHotelEmployees.add("Will Smith");
        myHotelEmployees.add("Benjamin McCartney");
        myHotelEmployees.add("Albert Davidson");
        myHotelEmployees.add("Jaden Clinton");
        myHotelEmployees.add("Donald Lincoln");
        myHotelEmployees.add("Bart Bureau");
        myHotelEmployees.add("Alistaire O'Doherty");

        System.out.println("The number of employees of the hotel is: " + myHotelEmployees.size());
        System.out.println("Their names are: " + myHotelEmployees);
        System.out.println("The employee at the first floor is: " + myHotelEmployees.get(1));

        myHotelEmployees.remove(1);
        System.out.println("The new list of employees is: " + myHotelEmployees);
        System.out.println("The new number of employees is:" + myHotelEmployees.size());

        for (String employeeName: myHotelEmployees) {
            System.out.println(employeeName);
        }

        myHotelEmployees.set(1, "Steve Irwin");
        myHotelEmployees.set(2, "Albert Davidson");
        myHotelEmployees.set(3, "Jaden Clinton");
        myHotelEmployees.set(4, "Donald Lincoln");
        myHotelEmployees.set(5, "Bart Bureau");
        myHotelEmployees.add(6, "Alistaire O'Doherty");

        System.out.println("The final list of employees is: " + myHotelEmployees);

    }
}
